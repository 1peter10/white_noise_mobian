#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pygame import mixer
import gi
import os
import sys
import time as t 
import threading

"""
Finally found threading information at
https://pygobject.readthedocs.io/en/latest/guide/threading.html
"""


gi.require_version("Gtk", "3.0")

from gi.repository import Gtk, Gio, Gdk, GLib
from gi.repository.GdkPixbuf import Pixbuf

storm_path = os.path.join(sys.path[0], "Audio", "storm_1_minute.ogg")

ocean_path = os.path.join(sys.path[0], "Audio", "ocean_1_minute.ogg")

underwater_path = os.path.join(sys.path[0], "Audio", "underwater.ogg")

white_noise_path = os.path.join(sys.path[0], "Audio", "white_noise.ogg")

pink_noise_path = os.path.join(sys.path[0], "Audio", "pink_noise.ogg")

pond_path = os.path.join(sys.path[0], "Audio", "crickets_at_pong.ogg")

dryer_path = os.path.join(sys.path[0], "Audio", "dryer_noise.ogg")

# Need to find audio for these two
purr_path = os.path.join(sys.path[0], "Audio", "dryer_noise.ogg")

jungle_path = os.path.join(sys.path[0], "Audio", "jungle_birds.ogg")


mixer.init()


def threaded(fn):
    def wrapper(*args, **kwargs):
        threading.Thread(target=fn, args=args, kwargs=kwargs).start()

    return wrapper


"""
Got the new icons and they look really nice
but there isn't a repeat button so I think
I will just make it repeat automatically
because it is for white noise.

Would be nice to add a timer or something to this...

JESUS FUCKING CHRIST
this is being super fucking annoying. In short the reason this isn't working
is because it just tells the program to sleep so then the programcan't do a thing
until its sleep ends. Now the way to get around that is to use threading and have 
that timer running on a different thread. The ISSUE IS that because the function 
for the timer relies on the input from the scrollview, you can't just call the function.
So this is where it gets FUCKING STUPID. Because the threading requires I call the function
and the function requires input from the scrollview, I can't fucking thread it.
 
Could also add an option to add your own white noise
and have it use shutil to move it into the proper
directory and see if that works?
"""



"""
About to do a huge rework here and change the UI where instead of a 
dropdown menu it is simplier and it will select a button for each white nise
and a volume slider on the button and the play/pause/stop buttons 
down there as well as the timer and volume slider

Gonna probably use an expander for the buttom buttons
"""


class white_noise(Gtk.Window):
    """
    need to add buttons to play and pause and stop
    as well as a repeat button for how many loops
    they want.
    """

    def __init__(self):
        Gtk.Window.__init__(self, title="White Noise")
        """
        Basic white noise app for Mobian
        as I want to build some more experience with
        this stuff and don't want to deal with having
        to request features on other people apps.
        """
        self.set_icon_from_file(os.path.join(sys.path[0], "icons", "cd.ico"))
        self.set_border_width(13)
        grid = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        self.add(grid)
        self.set_default_size(200, 400)  # Seems close to the right size

        # Play/Pause/Stop/Repeat Buttons
        play_button = Gtk.Button()
        play_image = Gtk.Image()
        play_image.set_from_file(
            os.path.join(sys.path[0], "icons", "play-button.svg")
        )
        play_button.add(play_image)
        play_button.connect("clicked", self.play)
        play_button.set_margin_top(5)
        play_button.set_margin_start(5)
        play_button.set_margin_start(5)
        
        pause_button = Gtk.Button()
        pause_image = Gtk.Image()
        pause_image.set_from_file(
            os.path.join(sys.path[0], "icons", "pause-button-24p(editted).ico")
        )
        pause_button.add(pause_image)
        pause_button.connect("clicked", self.pause)

        stop_button = Gtk.Button()
        stop_image = Gtk.Image()
        stop_image.set_from_file(
            os.path.join(sys.path[0], "icons", "stop.svg")
        )
        stop_button.add(stop_image)
        stop_button.connect("clicked", self.stop)
        stop_button.set_margin_top(5)
        stop_button.set_margin_start(5)
        stop_button.set_margin_start(5)

        repeat_button = Gtk.ToggleButton()
        repeat_image = Gtk.Image()
        repeat_image.set_from_file(os.path.join(sys.path[0], "icons", "stop-button-24p.ico"))
        repeat_button.add(repeat_image)
        repeat_button.connect("toggled", self.repeat)
        repeat_button.set_margin_top(5)

        # Choice box, shows the dropdown menu choices for the noise
        combo = Gtk.ComboBoxText()
        combo.connect("changed", self.playy)
        combo.append_text("Storm")
        combo.append_text("Ocean")
        combo.append_text("Underwater")
        combo.set_margin_top(5)
        combo.set_margin_start(5)
        combo.set_margin_start(5)

        # button options for white noise
        ocean_button = Gtk.ToggleButton(label="Ocean")
        storm_button = Gtk.ToggleButton(label="Storm")
        underwater_button = Gtk.ToggleButton(label="Underwater")
        white_noise_button = Gtk.ToggleButton(label="White Noise")
        pink_noise_button = Gtk.ToggleButton(label="Pink Noise")
        pond_button = Gtk.ToggleButton(label="Pond")
        dryer_button = Gtk.ToggleButton(label="Dryer")
        cat_purr_button = Gtk.ToggleButton(label="Cat Purr")
        jungle_button = Gtk.ToggleButton(label="Jungle")
        
        # functions called
        ocean_button.connect("toggled", self.ocean)
        storm_button.connect("toggled", self.storm)
        underwater_button.connect("toggled", self.underwater)
        white_noise_button.connect("toggled", self._white_noise)
        pink_noise_button.connect("toggled", self.pink_noise)
        pond_button.connect("toggled", self.pond)
        dryer_button.connect("toggled", self.dryer)
        cat_purr_button.connect("toggled", self.purr)
        jungle_button.connect("toggled", self.jungle)
        
        # labels for choice box, timer, volume button
        combo_label = Gtk.Label(label="Choose white noise")
        timer_label = Gtk.Label(label="Timer (minutes)")
        vol_label = Gtk.Label(label="Volume")
        self.sleep_label = Gtk.Label()
        combo_label.set_margin_top(5)
        combo_label.set_margin_start(5)

        timer_label.set_margin_top(5)
        timer_label.set_margin_start(5)

        vol_label.set_margin_top(5)
        vol_label.set_margin_start(5)

        # volume icon
        vol_image = Gtk.Image()
        vol_image.set_from_file(os.path.join(sys.path[0], "icons", "speaker_new.svg"))

        # stopwatch icon
        stopwatch = Gtk.Image()
        stopwatch.set_from_file(os.path.join(sys.path[0], "icons", "stopwatch.svg"))
        
        # Volume control stuff
        adjustments = Gtk.Adjustment(upper=100, step_increment=5, page_increment=10)
        self.vol_button = Gtk.SpinButton()
        self.vol_button.set_adjustment(adjustments)
        self.vol_button.connect("value-changed", self.vol)
        self.vol_button.set_value(30)
        self.vol_button.set_margin_top(5)
        self.vol_button.set_margin_start(5)
        self.vol_button.set_margin_start(5)
        # Fuck me this took forever to find but this fixed the issue of non centered buttons
        # It horizontally expands the buttons so it scales decently as well
        self.vol_button.set_hexpand(True)

        # Timer stuff
        timer_adjustments = Gtk.Adjustment(
            upper=1000, step_increment=5, page_increment=10
        )
        self.timer_but = Gtk.SpinButton()
        self.timer_but.set_adjustment(timer_adjustments)
        self.timer_but.set_value(5)
        self.timer_but.set_margin_top(5)
        self.timer_but.set_margin_start(5)
        self.timer_but.set_margin_start(5)

        # volume scale
        self.slider = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0.0, 100.0, 5.0)
        #slider.set_value_pos(Gtk.PositionType.BOTTOM)
        self.slider.set_value(30)
        self.slider.connect("value-changed", self.vol)
        self.slider.set_hexpand(True)
        
        self.timer_slider = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 1.0, 100.0, 5)
        self.timer_slider.set_value(15)
        self.timer_slider.set_hexpand(True)
        
        # Timer enter button
        self.timer_enter_button = Gtk.Switch()
        self.timer_enter_button.set_active(False)
        self.timer_enter_button.connect("notify::active", self.time)
        self.timer_enter_button.set_margin_top(5)
        self.timer_enter_button.set_margin_start(5)
        self.timer_enter_button.set_margin_start(5)
        self.timer_enter_button.set_property("width-request", 20)
        self.timer_enter_button.set_property("height-request", 10)

        # Attaching everything to the grid, sorted from top to bottom
        # (button added, column, row, column span, row span)
        #grid.attach(vol_image, 0, 23, 6, 1)
        #grid.attach_next_to(self.slider, vol_image, Gtk.PositionType.TOP, 10, 5)
        grid.attach(self.slider, 2, 19, 3, 1)#volume slider
        grid.attach_next_to(vol_image, self.slider, Gtk.PositionType.LEFT, 2, 2)
        grid.attach(ocean_button, 1, 7, 2, 1)
        grid.attach(underwater_button, 3, 7, 2, 1)
        grid.attach(storm_button, 1, 9, 2, 1)
        grid.attach(white_noise_button, 3, 9, 2, 1)
        grid.attach(pink_noise_button, 1, 11, 2, 1)
        grid.attach(pond_button, 3, 11, 2, 1)
        grid.attach(dryer_button, 1, 13, 2, 1)
        #grid.attach(cat_purr_button, 3, 13, 2, 1)
        grid.attach(jungle_button, 3, 13, 2, 1)
        # manual placement of buttons
        grid.attach(play_button, 1, 35, 2, 1)
        grid.attach_next_to(stop_button, play_button, Gtk.PositionType.RIGHT, 2, 2)
        #grid.attach(stopwatch, 0, 29, 6, 1)
        grid.attach(self.timer_slider, 2, 27, 3, 1)
        grid.attach(self.timer_enter_button, 2, 31, 2, 1)
        grid.attach_next_to(stopwatch, self.timer_slider, Gtk.PositionType.LEFT, 1, 1)
        #grid.attach(self.sleep_label, 0, 33, 6, 3)

        # Jesus christ, finally got this working!
        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(
            screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        # stuff needs to be lowercase for what it is in Gtk but without Gtk
        css_path = os.path.join(sys.path[0], "main.css")
        provider.load_from_path(css_path)

    def play(self, widget):
        """
        Plays the white noise
        Habving an issue where it won't play after
        stopping. I don't exactly know how to have it
        deal with unpausing and also starting from stop.
        Though it might not matter since this is all white
        noise so there isn't any real continuity.
        """
        try:
            mixer.music.unpause()
        except:
            mixer.music.play(loops=-1)
        print("Play button is working")

    def playy(self, widget):
        if widget.get_active_text() == "Storm":
            # mixer.music.stop()
            if (
                mixer.music.get_busy() == True
            ):  # This returns true if music is actively playing.
                mixer.music.stop()
            mixer.music.load(storm_path)
            mixer.music.play(loops=-1)

        if widget.get_active_text() == "Ocean":
            # mixer.music.stop()
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(ocean_path)
            mixer.music.play(loops=-1)

        if widget.get_active_text() == "Underwater":
            # mixer.music.stop()
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(underwater_path)
            mixer.music.play(loops=-1)

    def pause(self, widget):
        """
        Pauses the white noise
        """
        mixer.music.pause()
        print("Pause button is working")

    def stop(self, widget):
        """
        Stops the white noise
        """
        mixer.music.stop()
        print("Stop button is working")

    def repeat(self, button):
        """
        Puts the white noise on repeat.
        Need to make this into a toggle button
        so the user can see when repeat is on.
        """
        if button.get_active():
            state = "on"
            mixer.music.play(loops=-1)
        else:
            state = "off"
            mixer.music.play(loops=0)
        # mixer.music.play(loops=-1)
        print("Repeat button is working")

    def vol(self, slider):
        """
        Volume control. Apparently switches back each
        the volume when the music is changed but that
        is a fundamental thing that pygame does so
        I can't really do anything about it.
        """
        #print(self.slider.get_value())
        volume_level = int(self.slider.get_value())
        print(volume_level)
        mixer.music.set_volume(volume_level)

    """
    Need to update timer info for ScrollButton to show timer is going down!    
    """

    @threaded
    def time(self, switch, gparam):
        num = self.timer_slider.get_value()
        num = int(num)
        print(num)
        # self.sleep_label.set_text(f"Timer is active\nand will shut down\n in {num} minutes!")
        md_string = f"<i>Timer is active and will\nshut down in {num} minutes!\nNOTE:\nOnce this has started it\ncan't be reset or stopped.</i>"
        #self.sleep_label.set_markup(f"{md_string}")
        if switch.get_active():
            sleep_time = num * 60
            t.sleep(sleep_time)
            GLib.idle_add(self.end)
        # maybe make a for loop and have idle_add update the timer inside
        # the for loop and then outside the loop it calls to close the program

    # can't get this function to work for some reason
    def msg_box(self):
        dialog_info = "Your timer has started! "
        md = Gtk.MessageDialog(
            transient_for=self,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Your timer has started! Ufnor",
        )
        md.run()
        md.destroy()

    def end(self):
        Gtk.main_quit()

    # Where the functions to play the specific music is

    def storm(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(storm_path)
            mixer.music.play(loops=-1)

    def ocean(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(ocean_path)
            mixer.music.play(loops=-1)

    def underwater(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(underwater_path)
            mixer.music.play(loops=-1)

    def _white_noise(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(white_noise_path)
            mixer.music.play(loops=-1)

    def pink_noise(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(pink_noise_path)
            mixer.music.play(loops=-1)

    def pond(self, button):
        print("Called")
        if button.get_active():
            print("Active")
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(pond_path)
            mixer.music.play(loops=-1)

    def dryer(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(dryer_path)
            mixer.music.play(loops=-1)

    def purr(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(purr_path)
            mixer.music.play(loops=-1)

    def jungle(self, button):
        if button.get_active():
            if mixer.music.get_busy() == True:
                mixer.music.stop()
            mixer.music.load(jungle_path)
            mixer.music.play(loops=-1)
        
        
if __name__ == "__main__":
    window = white_noise()
    window.connect("destroy", Gtk.main_quit)
    window.show_all()
    Gtk.main()
# print(help(white_noise))
